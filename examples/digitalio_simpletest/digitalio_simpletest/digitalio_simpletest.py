# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

from framework import button, board_led


SLEEP = 0.01

def button_callback():
    board_led.value = not board_led.value

def setup():
    button.register_callback('event', button_callback)

def loop():
    pass
