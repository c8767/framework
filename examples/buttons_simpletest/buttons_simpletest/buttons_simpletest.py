# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

from framework import buttons
from framework import Application
import asyncio

class App(Application):
    async def run(self):
        while True:
            if buttons.pin_alias is not None:
                print(buttons.pin_alias)
                buttons.pin_alias = None
            await asyncio.sleep(self.sleep)
