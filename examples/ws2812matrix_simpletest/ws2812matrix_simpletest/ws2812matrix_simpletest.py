# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

from framework import led_matrix
from framework import Application
import asyncio
from random import randrange

class App(Application):
    async def run(self):
        while True:
            i = randrange(8)
            j = randrange(8)
            k = randrange(8)
            m = randrange(16)
            n = randrange(16)
            o = randrange(16)
            led_matrix.clear = True
            led_matrix.pixels = {(i, j,): (m, n, o),
                                 (j, k,): (n, o, m),
                                 (k, i,): (o, m, n)}
            await asyncio.sleep(0.5)
