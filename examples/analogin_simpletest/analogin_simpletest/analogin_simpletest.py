# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

from framework import pot0
from framework import Application
import asyncio

class App(Application):
    async def run(self):
        self.sleep = 1.0
        while True:
            print(pot0.value)
            await asyncio.sleep(self.sleep)
