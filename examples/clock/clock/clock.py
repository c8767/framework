# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

from framework import led_matrix, brightness, rtc, button0
from framework import Application
import asyncio

class App(Application):
    async def run(self):
        if False:
            rtc.set = (2022, 5, 20, 1, 41, 0, 4, -1, 0)
        while True:
            led_matrix.text = {(1, 1): f'{rtc.hour:02d}:{rtc.minute:02d}'}
            led_matrix.brightness = 16 * brightness.value // 2**16
            if button0.value:
                led_matrix.text = {(7, 1): f'{rtc.abr}'}
            await asyncio.sleep(self.sleep)
