# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

from framework import board_led, clock


def invert():
    board_led.value = not board_led.value

def setup():
    clock.register_callback('event', invert)

def loop():
    pass
