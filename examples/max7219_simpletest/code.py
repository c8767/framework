# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

from framework import AppLoader

AppLoader(app='max7219_simpletest')
