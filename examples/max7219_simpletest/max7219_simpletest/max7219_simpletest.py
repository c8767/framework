# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

from framework import led_matrix, brightness, position
from framework import Application
import asyncio

class App(Application):
    async def run(self):
        led_matrix.clear_all = True
        while True:
            pixels = {}
            for i in range(8):
                pixels[(32 * position.value//(2**16), i)] = 1
            led_matrix.pixels = pixels
            led_matrix.brightness = 16 * brightness.value // 2**16
            await asyncio.sleep(self.sleep)
