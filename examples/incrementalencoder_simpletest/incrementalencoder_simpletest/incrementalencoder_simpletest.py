# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

from framework import encoder
from framework import Application
import asyncio

class App(Application):
    async def run(self):
        _position = encoder.position
        while True:
            if _position != encoder.position:
                print(encoder.position)
                _position = encoder.position
            await asyncio.sleep(self.sleep)
