# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

from framework import edge_counter
from framework import Application
import asyncio

class App(Application):
    async def run(self):
        _cnt = edge_counter.count
        while True:
            if _cnt != edge_counter.count:
                print(edge_counter.count)
                _cnt = edge_counter.count
            
            if edge_counter.count >= 10:
                edge_counter.reset = True
                
            await asyncio.sleep(self.sleep)
