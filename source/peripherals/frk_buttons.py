import keypad
import asyncio
from framework import Peripheral

class Buttons(Peripheral):
    def set_defaults(self):
        self.sleep = 0.01
        self.value_when_pressed = True
        self.pull = True

        self.key_number = None
        self.pin_alias = None
        self.pin_name = None

        self.pressed = False
        self.released = True
        self.event = False

        self.callbacks = {'event': [],
                          'pressed': [],
                          'released': []}

    async def run(self):
        with keypad.Keys(self.pins, value_when_pressed=self.value_when_pressed, pull=self.pull) as buttons:
            while True:
                event = buttons.events.get()
                if event:
                    if event.pressed:
                        self.pressed = True
                        self.released = False
                        self.key_number = event.key_number
                        self.pin_alias = self.raw_aliased_pins[event.key_number][0]
                        self.pin_name = self.raw_aliased_pins[event.key_number][1]
                        self._event_handler('pressed')
                    else:
                        self.pressed = False
                        self.released = True
                        self._event_handler('released')
                    self._event_handler('event')
                await asyncio.sleep(self.sleep)
