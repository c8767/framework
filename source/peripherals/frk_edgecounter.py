# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

import countio
import asyncio
import digitalio
from framework import Peripheral

class EdgeCounter(Peripheral):
    edges = {'RISE': countio.Edge.RISE,
             'FALL': countio.Edge.FALL,
             'RISE_AND_FALL': countio.Edge.RISE_AND_FALL}
    pulls = {'NONE': None,
             'UP': digitalio.Pull.UP,
             'DOWN': digitalio.Pull.DOWN}

    def set_defaults(self):
        self.sleep = 0
        self.pull = 'UP'
        self.edge = 'FALL'

        self.count = 0
        self.reset = False
        self.event = True

        self.callbacks = {'event': []}

    async def run(self):
        edge = self.edges[self.edge]
        pull = self.pulls[self.pull]
        _count = self.count
        with countio.Counter(self.pin, edge=edge, pull=pull) as counter:
            while True:
                self.count = counter.count
                if _count != self.count:
                    self._event_handler('event')
                    _count = self.count

                if self.reset:
                    counter.reset()
                    self.reset = False
                await asyncio.sleep(self.sleep)
