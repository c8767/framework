# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

import neopixel
import asyncio

class WS2812Matrix:
    def set_defaults(self):
        self.sleep = 0.01
        self.width = 8
        self.height = 8
        self.pixels = None
        self.bpp = 3
        self.brightness = 1.0
        self.auto_write = True
        self.pixel_order = 'GRB'
        self.fill = None
        self.clear = False

    async def run(self):
        pixel_count = self.height * self.width
        with neopixel.NeoPixel(self.pin, pixel_count, bpp=self.bpp, brightness=self.brightness, auto_write=self.auto_write, pixel_order=self.pixel_order) as pixels:
            while True:
                if self.pixels is not None:
                    for x_y, value in self.pixels.items():
                        position = x_y[0] + x_y[1] * self.width
                        pixels[position] = value

                if self.fill is not None:
                    pixels.fill(self.fill)
                    self.fill = None

                if self.clear:
                    pixels.fill((0, 0, 0))
                    self.clear = False

                pixels.show()
                await asyncio.sleep(self.sleep)
