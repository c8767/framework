# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

import time
import board
import busio
import adafruit_ds3231
import asyncio

class DS3231RTC:
    def set_defaults(self):
        self.sleep = 1.0
        self.frequency = 100000
        self.timeout = 255

        self.now = None
        self.year = 0
        self.month = 0
        self.day_of_month = 0
        self.hour = 0
        self.minute = 0
        self.second = 0
        self.day_of_week = 0
        self.day_of_year = -1
        self.dst = -1

        self.set = None

        self.date = ''
        self.time = ''
        self.day = ''
        self.abr = ''

    async def run(self):
        days = ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday')
        abr = ('MON', 'TUES', 'WED', 'THUR', 'FRI', 'SAT', 'SUN')
        SCL = self.aliased_pins['SCL']
        SDA = self.aliased_pins['SDA']
        with busio.I2C(SCL, SDA, frequency=self.frequency, timeout=self.timeout) as i2c:
            rtc = adafruit_ds3231.DS3231(i2c)
            while True:
                if self.set is not None:
                    rtc.datetime = time.struct_time(self.set)
                    self.set = None

                t = rtc.datetime
                self.now = t
                self.year = int(t.tm_year)
                self.month = int(t.tm_mon)
                self.day_of_month = int(t.tm_mday)
                self.hour = int(t.tm_hour)
                self.minute = int(t.tm_min)
                self.second = int(t.tm_sec)
                self.day_of_week = int(t.tm_wday)
                self.day_of_year = int(t.tm_yday)
                self.dst = int(t.tm_isdst)
                self.date = f'{self.day_of_month}/{self.month}/{self.year}'
                self.time = f'{self.hour:2}:{self.minute:2}:{self.second:2}'
                self.day = days[self.day_of_week]
                self.abr = abr[self.day_of_week]

                await asyncio.sleep(self.sleep)
