# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

import analogio
import asyncio

class AnalogIn:
    def set_defaults(self):
        self.sleep = 0.01
        self.reference_voltage = 0
        self.value = 0

    async def run(self):
        with analogio.AnalogIn(self.pin) as a_in:
            self.reference_voltage = a_in.reference_voltage
            while True:
                self.value = a_in.value
                await asyncio.sleep(self.sleep)
