# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

import board
import busio
import digitalio
import asyncio
from adafruit_max7219 import matrices

class MAX7219Matrix:
    def set_defaults(self):
        self.sleep = 0.01
        self.width = 8
        self.height = 8
        self.rotation = 1
        self.clear_all = False
        self.pixels = {}
        self.enable = True
        self.fill = None
        self.brightness = None
        self.text = {}
    
    async def run(self):
        CS = self.aliased_pins['CS']
        SCK = self.aliased_pins['SCK']
        MOSI = self.aliased_pins['MOSI']
        with digitalio.DigitalInOut(CS) as cs:
            with busio.SPI(SCK, MOSI=MOSI) as spi:
                matrix = matrices.CustomMatrix(spi, cs, self.width, self.height, rotation=self.rotation)
                while True:
                    if self.clear_all:
                        self.clear_all = False
                        matrix.clear_all()
                    
                    if self.enable:
                        matrix.clear_all()
                        if len(self.pixels) > 0:
                            for pos, value in self.pixels.items():
                                matrix.pixel(pos[0], pos[1], value)
                    
                    if self.fill is not None:
                        matrix.fill(self.fill)
                    
                    if self.brightness is not None:
                        matrix.brightness(self.brightness)
                    
                    if len(self.text) > 0:
                        for pos, text in self.text.items():
                            matrix.text(text, pos[0], pos[1])
                    
                    matrix.show()
                    await asyncio.sleep(self.sleep)
