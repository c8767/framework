# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

import asyncio
from framework import Peripheral

class FreeRun(Peripheral):
    def set_defaults(self):
        self.initial_value = False
        self.value = False
        self.t_on = 0.5
        self.t_off = 0.5

        self.event = False
        self.rising = False
        self.falling = False

        self.callbacks = {'event': [],
                          'rising': [],
                          'falling': []}

    async def run(self):
        self.value = self.initial_value
        while True:
            self.value = not self.value
            if self.value:
                sleep = self.t_on
                self._event_handler('rising')
            else:
                sleep = self.t_off
                self._event_handler('falling')
            self._event_handler('event')
            await asyncio.sleep(sleep)
