# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

import digitalio
import asyncio

class DigitalOut:
    drive_modes = {'NONE': None,
                   'PUSH_PULL': digitalio.DriveMode.PUSH_PULL,
                   'OPEN_DRAIN': digitalio.DriveMode.OPEN_DRAIN}

    def set_defaults(self):
        self.drive_mode = 'PUSH_PULL'
        self.sleep = 0
        self.initial_value = False
        self.value = False
        self.invert = False
        self.enable = True

    async def run(self):
        dm = self.drive_modes[self.drive_mode]
        self.value = self.initial_value
        with digitalio.DigitalInOut(self.pin) as d_out:
            d_out.switch_to_output(value=self.initial_value, drive_mode=dm)
            while True:
                d_out.value = (self.value ^ self.invert) & self.enable
                await asyncio.sleep(self.sleep)
