# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

import pwmio
import asyncio

class PWMOut:
    def set_defaults(self):
        self.sleep = 0.05
        self.duty_cycle = 0
        self.frequency = 500
        self.variable_frequency = False

    async def run(self):
        dc = self.duty_cycle
        f = self.frequency
        vf = self.variable_frequency
        with pwmio.PWMOut(self.pin, duty_cycle=dc, frequency=f, variable_frequency=vf) as pwmo:
            while True:
                pwmo.duty_cycle = self.duty_cycle
                if self.variable_frequency:
                    pwmo.frequency = self.frequency
                await asyncio.sleep(self.sleep)
