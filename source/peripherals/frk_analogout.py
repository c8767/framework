# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

import analogio
import asyncio

class AnalogOut:
    def set_defaults(self):
        self.sleep = 0.01
        self.value = 0

    async def run(self):
        with analogio.AnalogOut(self.pin) as a_out:
            while True:
                a_out.value = self.value
                await asyncio.sleep(self.sleep)
