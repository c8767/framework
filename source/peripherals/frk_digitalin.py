# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

import digitalio
import asyncio
from framework import Peripheral

class DigitalIn(Peripheral):
    pull_modes = {'NONE': None,
                  'UP': digitalio.Pull.UP,
                  'DOWN': digitalio.Pull.DOWN}

    def set_defaults(self):
        self.sleep = 0
        self.pull = 'UP'
        self.invert = False

        self.value = False
        self.event = False
        self.rising = False
        self.falling = False

        self.callbacks = {'event': [],
                          'rising': [],
                          'falling': []}

    async def run(self):
        with digitalio.DigitalInOut(self.pin) as d_in:
            d_in.switch_to_input(pull=self.pull_modes[self.pull])
            _value = d_in.value
            while True:
                self.value = d_in.value ^ self.invert
                if not _value and self.value:
                    self._event_handler('rising')
                if _value and not self.value:
                    self._event_handler('falling')
                if _value != self.value:
                    self._event_handler('event')
                    _value = self.value
                await asyncio.sleep(self.sleep)
