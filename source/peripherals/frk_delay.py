# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

import asyncio
from framework import Peripheral

class Delay(Peripheral):
    def set_defaults(self):
        self.sleep = 0
        self.initial_value = False
        self.value = self.initial_value

        self.delay = 0.5
        self.start = False
        self.started = False
        self.event = False

        self.callbacks = {'event': []}

    async def run(self):
        self.value = self.initial_value
        while True:
            if self.started:
                self.value = not self.value
                self._event_handler('event')
                self.started = False
                sleep = self.sleep
            elif self.start and not self.started:
                self.start = False
                self.started = True
                sleep = self.delay
            else:
                sleep = self.sleep
            await asyncio.sleep(sleep)
