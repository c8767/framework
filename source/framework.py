# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

import board
import sys
import asyncio
import gc

class AppLoader:
    def __new__(cls, app='default_app'):
        instance = super(AppLoader, cls).__new__(cls)
        instance.__init__(app)
        gc.collect()
        asyncio.run(instance.main())

    def __init__(self, app):
        try:
            import json
            with open(f'./{app}/{app}.json') as j:
                configurations = json.loads(j.read())
        except:
            import msgpack
            with open(f'./{app}/{app}.mpk', 'rb') as m:
                configurations = msgpack.unpack(m)

        self.tasks = []
        for alias, options in configurations.items():
            instance = getattr(__import__(f'frk_{options['peripheral_type']}'.lower()), options['peripheral_type'], None)()
            instance.alias = alias
            instance.app_name = app
            instance.set_defaults()

            for option, value in options.items():
                if option is not 'pin' and option is not 'pins':
                    setattr(instance, option, value)
                else:
                    raw_aliased_pins = sorted((v, k) for k, v in value.items())
                    instance.raw_aliased_pins = [(k, v) for v, k in raw_aliased_pins]
                    instance.aliased_pins = {k: getattr(board, v, None) for k, v in value.items()}
                    if len(value) == 1:
                        instance.pin = getattr(board, list(value.values())[0], None)
                        instance.pins = [instance.pin]
                    if len(value) > 1:
                        instance.pins = [getattr(board, pin, None) for pin in sorted(value.values())]

            globals()[alias] = instance
            self.tasks.append(asyncio.create_task(instance.run()))

        sys.path.insert(0, f'./{app}')
        globals()[app] = __import__(app)
        app_class = getattr(globals()[app], 'App', None)
        if app_class is not None:
            app_instance = app_class()
            app_instance.app_name = app
            app_instance.set_defaults()
            app_instance.register_callbacks()
            self.tasks.append(app_instance.run())
        else:
            setattr(globals()[app], 'app_name', app)
            setup = getattr(globals()[app], 'setup', None)
            loop = getattr(globals()[app], 'loop', None)
            sleep = getattr(globals()[app], 'SLEEP', 0.01)
            if setup is not None and loop is not None:
                setup()
                self.tasks.append(self.run(loop, sleep))
            elif setup is None and loop is not None:
                self.tasks.append(self.run(loop, sleep))
            elif setup is not None and loop is None:
                setup()
                self.tasks.append(self.run(self._do_nothing, sleep))
            else:
                self.tasks.append(getattr(globals()[app], 'run', None)())

    def _do_nothing(self):
        pass

    async def run(self, func, sleep):
        while True:
            func()
            await asyncio.sleep(sleep)
            

    async def main(self):
        await asyncio.gather(*self.tasks)


class Application:
    def __init__(self):
        self.sleep = 0.01

    def set_defaults(self):
        pass

    def register_callbacks(self):
        pass

    async def run(self):
        while True:
            await asyncio.sleep(self.sleep)


class Peripheral:
    def __init__(self):
        pass

    def set_defaults(self):
        pass

    def register_callback(self, name, func):
        self.callbacks[name].append(func)

    def _run_callbacks(self, name):
        for callback in self.callbacks[name]:
            callback()
            setattr(self, name, False)

    def _event_handler(self, event_type):
        self.event = True
        setattr(self, event_type, True)
        self._run_callbacks(event_type)

    async def run(self):
        raise NotImplementedError
