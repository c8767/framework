# Contributions are welcome. 

## Especially needed is help with:

* Documentation

* Tutorials

* Peripherals -- I don't have that much hardware that I can code for and test...

## Bug Reports

Please report any bugs you find. If you can, include a description of the bug and some example code so I can reproduce the failure.

## Feature Requests

Feature requests are always welcome. Please include:

* A description of the feature

* A use case for the feature

* Optionally, example code of what you're trying to do

### Peripheral Requests

I have the hardware that I have. In general, if there is an existing CircuitPython library or module for the hardware, I can make an attempt at making it work with `framework`. I see a few ways this could work:

* Writing a peripheral isn't too difficult. I can walk you through it if necessary

* I write the code, you test the code on your hardware and let me know the results so it can be added to the repo

* Have the hardware shipped to me (note that I live in Beijing)

* Make a donation so that I can buy the hardware (if readily available. I usually buy stuff from Taobao so check there first)

### Porting CircuitPython to BoardX

`framework` is designed to be as board agnostic as possible. If it runs CircuitPython, `framework` should work. If your board doesn't run CircuitPython... that's not really my thing.

# Future Goals

* CircuitPython/MicroPython ambiguity or MicroPython port

* Usability in CPython and installable from [pypy](https://www.pypy.org/)

* `constructor for framework` -- a GUI app and IDE to build framework projects (thinking about [Kivy](https://kivy.org/#home) possibly)
