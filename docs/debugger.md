# Simple Debugger

There is a simple debugger available with `framework`. In order to use the debugger, 'frk_debugger' must be in the 'CIRCUITPY/lib' folder.

## Global Debugger

The global debugger, once imported and activated, can be used anywhere in your project. This includes your app, peripherals, even `framework` itself. There are three steps to setting up the global debugger:

1. from frk_debugger import d

2. Set the alias of the debugger with `d.alias` (optional -- the default alias is 'GLOBAL')

3. Activate the debugger with `d.act = True`

The global debugger is now ready to use in your project. The best practice is to follow these steps in 'code.py' as that is the entry point for your project. This will start the debugger at the earliest possible time:

    from framework import AppLoader
    from frk_debugger import d

    d.act = True
    d.debug('Starting app')
    Apploader(app='blink')

To use `d`, import it into your code and use `d.debug()` (this takes a single argument that is a string or can be parsed to a string):

    from framework import board_led, clk
    from frk_debugger import d

    d.debug('Entered app')

    def debug_rising():
        d.debug('Rising edge detected')

    def setup():
        d.debug('Setting up')
        _value = clk.value
        clk.register_callback('rising', debug_rising)
    
    def loop():
        if _value != clk.value:
            d.debug('EVENT!')
            _value = clk.value

In the console, you will see:

```
GLOBAL: Starting app
GLOBAL: Entered app
GLOBAL: Setting up
GLOBAL: Rising edge detected
GLOBAL: EVENT!
...
```

## Local Debugger

The local debugger is used in a similar fashion to the global debugger with the caveats that the alias should be set and it will only be available in the scope that it is declared or lower:

    from framework import board_led, clk
    from frk_debugger import Debugger

    local_d = Debugger(act=True)
    local_d.alias = 'my_debugger'
    local_d.debug('Started local debugging')

    def setup():
        local_d.debug('This app does nothing')
