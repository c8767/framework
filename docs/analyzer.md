# Measure the Execution Time of Code

There is a simple code profiler that can be used with `framework`. In order to use the profiler, 'frk_analyzer' must be copied to the 'CIRCUITPY/lib' folder. The code profiler can be used globally and locally.

## Global Profiler

The global profiler, once imported and activated, can be used anywhere in your project. This includes your app, peripherals, even framework itself. There are three steps to setting up the global profiler:

* from frk_analyzer import a

* Set the alias of the debugger with a.alias (optional -- the default alias is 'GLOBAL')

* Activate the debugger with a.act = True


The global profiler is now ready to use in your project. The best practice is to follow these steps in 'code.py' as that is the entry point for your project. This will start the profiler at the earliest possible time:

    from framework import AppLoader
    from frk_analyzer import a

    a.act = True
    a.mark('APP START')

    AppLoader(app='blink')

To use `a`, import it into your code and use `a.mark('my_mark')` and `a.delta('my_mark')`:

    from framework import board_led, clk
    from frk_analyzer import a

    a.delta('APP_START')
    a.mark('my_mark')

    def invert():
        board_led.value = not board_led.value
    
    def setup():
        clk.register_callback('event', invert)
    
    def loop()
        a.delta('my_mark')

## Local Profiler

The local profiler is used in a similar fashion to the global profiler with the caveats that the alias should be set and it will only be available in the scope that it is declared or lower:

    from framework import board_led, clk
    from frk_analyzer import Analyzer

    local_a = Analyzer(act=True)
    local_a.alias = 'my_profiler'
    local_a.mark('Started local profiler')

    def setup():
        local_a.delta('Started local profiler')
