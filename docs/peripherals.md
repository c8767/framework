# Peripherals

`framework` peripherals are Python (.py or .mpy) files that contain a peripheral class, are accessible to `framework`, and have a file name based on the class name. File names are prefixed with 'frk_' and must be the lower case version of the class name (eg. 'DigitalIn' -> 'frk_digitalin'). Peripherals may have zero or more pins, zero or more defaults or parameters, and zero or more callback triggers.

## Peripheral Philosophy

`framework` peripherals should be as self contained as possible. 

A peripheral should 'just work' with as little initial intervention as possible. This means that reasonable defaults should be set by the peripheral designer from the beginning. 

Peripherals should be as flexible as possible; offering (but not requiring!) options from configuration to runtime. 

APIs should follow in the footsteps of previous designers. If a peripheral is based on another library or module, stick to the language of that library or module as closely as possible. A user should be able to use the library/module documentation to get up and running without knowing the internals of the peripheral.

Peripherals should not use 'jargon'. Whenever possible, use keywords and values that can be taken *directly* from JSON. Strings, ints, floats, and bool work great. Dicts and lists work well too.

Use contexts (`with`) whenever possible. CircuitPython does not like it when pins are not shut down properly. If there isn't a context, do your best.

## The Peripheral Class

Peripheral classes must implement a `set_defaults` method and an `async` `run` method. If necessary for callbacks, a `register_callback` method must be implemented and a `_run_callbacks` method and `_event_handler` method can be implemented. However, especially if using callbacks, it is recommended to use the provided `Peripheral` base class and override methods as necessary.

The `set_defaults` method is the first to be called. It is called before settings from the configurations file are applied. Any setting or parameter that is needed for the peripheral to run must be set here with the expectation that none, any, or all of those settings may be overwritten at any time during the life of the peripheral *except for pins*.

The `app_name` and peripheral `alias` are set in the instance.

The next step is that the default settings are overwritten with optional settings specified in the configurations file. At this time, pin/pins are also set up.

### `pin` and `pins`

If "pin" or "pins" is specified in the configurations file, `framework` will provide access to the class instance in three or four ways:

* `raw_aliased_pins` -- a list of `(pin_alias, pin_name)` tuples, alpha-numerically sorted by pin_name, where pin_alias and pin_name are strings

* `aliased_pins` -- a dict in the form `{pin_alias: board_pin}` where pin_alias is a string and board_pin is derived from `board`

* `pins` -- a list of `board_pin`s sorted alpha-numerically by pin_name where board_pin is derived from `board`

* `pin` -- A `board_pin` derived from `board`. *Only available if a single pin is specified in the configurations file*

**Note:** In the configurations file, there is no difference between "pin" or "pins". Pins are counted during the `framework` peripheral class initialization and handled accordingly.

### A simple peripheral

This is the `AnalogIn` peripheral (frk_analogin). It has one input pin and three parameters:

    import analogio
    import asyncio

    class AnalogIn:
        def set_defaults(self):
            self.sleep = 0.01
            self.reference_voltage = 0
            self.value = 0
        
        async def run(self):
            with analogio.AnalogIn(self.pin) as a_in:
                self.reference_voltage = a_in.reference_voltage
                while True:
                    self.value = a_in.value
                    await asyncio.sleep(self.sleep)

### A simple peripheral with callbacks

This is the `DigitalIn` peripheral (frk_digitalin). It uses the `Peripheral` base class and callbacks.

    import digitalio
    import asyncio
    from framework import Peripheral

    class DigitalIn(Peripheral):
        pull_modes = {'NONE': None,
                      'UP': digitalio.Pull.UP,
                      'DOWN': digitalio.Pull.DOWN}

        def set_defaults(self):
            self.sleep = 0
            self.pull = 'UP'
            self.invert = False

            self.value = False
            self.event = False
            self.rising = False
            self.falling = False

            self.callbacks = {'event': [],
                              'rising': [],
                              'falling': []}

        async def run(self):
            with digitalio.DigitalInOut(self.pin) as d_in:
                d_in.switch_to_input(pull=self.pull_modes[self.pull])
                _value = d_in.value
                while True:
                    self.value = d_in.value ^ self.invert
                    if not _value and self.value:
                        self._event_handler('rising')
                    if _value and not self.value:
                        self._event_handler('falling')
                    if _value != self.value:
                        self._event_handler('event')
                        _value = self.value
                    await asyncio.sleep(self.sleep)

### An SPI peripheral

This is the MAX7219 peripheral (frk_max7219matrix). It uses SPI ('CS', 'SCK', 'MOSI').

    import board
    import busio
    import digitalio
    import asyncio
    from adafruit_max7219 import matrices

    class MAX7219Matrix:
        def set_defaults(self):
            self.sleep = 0.01
            self.width = 8
            self.height = 8
            self.rotation = 1
            self.clear_all = False
            self.pixels = {}
            self.enable = True
            self.fill = None
            self.brightness = None
            self.text = {}
    
        async def run(self):
            CS = self.aliased_pins['CS']
            SCK = self.aliased_pins['SCK']
            MOSI = self.aliased_pins['MOSI']
            with digitalio.DigitalInOut(CS) as cs:
                with busio.SPI(SCK, MOSI=MOSI) as spi:
                    matrix = matrices.CustomMatrix(spi, cs, self.width, self.height, rotation=self.rotation)
                    while True:
                        if self.clear_all:
                            self.clear_all = False
                            matrix.clear_all()
                    
                        if self.enable:
                            matrix.clear_all()
                            if len(self.pixels) > 0:
                                for pos, value in self.pixels.items():
                                    matrix.pixel(pos[0], pos[1], value)
                    
                        if self.fill is not None:
                            matrix.fill(self.fill)
                    
                        if self.brightness is not None:
                            matrix.brightness(self.brightness)
                    
                        if len(self.text) > 0:
                            for pos, text in self.text.items():
                                matrix.text(text, pos[0], pos[1])
                    
                        matrix.show()
                        await asyncio.sleep(self.sleep)
