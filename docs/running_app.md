# Running Your App

In general, the entry point for apps is in `code.py`. `framework` has a class named `AppLoader` that handles all configured peripherals and whatever app is specified. `AppLoader` takes a single keyword argument: `app`

    # code.py
    from framework import AppLoader

    AppLoader(app='my_app')

If the `app` keyword is not used, 'default_app' will be loaded and run:

    AppLoader()
