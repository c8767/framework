# Project Organization

Project organization is strictly enforced by `framework`. Projects consist of three main parts:

* A project folder in the root directory of CIRCUITPY. The folder name is the name of your project, eg. 'my_app'

* A configurations file that has the same name as the project folder and is in the project folder. It must be a JSON (.json) or messagepack (.mpk) file.

* An application file that has the same name as the project folder and is in the project folder. It must be a Python (.py or .mpy) file.

Optionally there may be other files in the project folder, eg. data files, fonts, etc. `framework` will only look in a specified folder for the configurations file and application file and ignore all else. 

A minimal example:

```
CIRCUITPY
|-- code.py
|-- lib
|-- my_app
|---- my_app.json
|---- my_app.py
```

## lib

### Dependencies

In order for your `framework` project to work, the following libraries/modules must be in the 'CIRCUITPY/lib' folder:

* asyncio

* adafruit_ticks

* framework

**Note:** *asyncio* and *adafruit_ticks* can be found in the [Adafruit libraries bundle](https://circuitpython.org/libraries).

While many peripherals are based on [CircuitPython builtin modules](https://docs.circuitpython.org/en/latest/shared-bindings/index.html#modules), there are other peripherals that have their own dependencies. Check the code or documentation for each peripheral for its requirements.
