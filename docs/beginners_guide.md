**THIS DOCUMENT IS DEPRECATED**

It may still work and may still provide valuable insight for how `framework` works, but refer to the other docs for an easier time...

# What is framework?

`framework` is an app framework for [microcontrollers running CircuitPython](https://circuitpython.org/downloads) using [cooperative multitasking](https://learn.adafruit.com/cooperative-multitasking-in-circuitpython-with-asyncio). It helps organize projects. It loads apps.
It makes peripherals easy and reusable.

Because `framework` runs on top of [CircuitPython](https://circuitpython.org/), make sure [you're familiar with and set up for CircuitPython](https://learn.adafruit.com/welcome-to-circuitpython) before starting.

# The Complete Beginner's Guide To framework

## Steps to a completed framework project

1. Make a configurations file

2. Write your app

3. Ensure that your project can access libraries, modules, and data files

4. Enable your app in 'code.py'

5. Launch!

## Starting a framework project

Imagine that we want to make a digital clock. The first thing to do is start thinking about what we want the clock to do. Obviously it should
measure and show the time. We also want it to show the day of the week when we push a button. We've also noticed that digital clocks can be too dim
during the day and too bright at night. So we want some way to control the brightness as well.

Now we can start to gather the parts. Digging through our parts box, we find that we have a [Raspberry Pi Pico](https://www.raspberrypi.com/documentation/microcontrollers/raspberry-pi-pico.html), a [MAX7219 32x8 LED Matrix](https://www.tinytronics.nl/shop/en/lighting/matrix/led-matrix-32x8-with-max7219-module), a [DS3231 Real Time Clock](https://learn.adafruit.com/adafruit-ds3231-precision-rtc-breakout/overview), and various pushbuttons and potentiometers and wires...

The next step is to start wiring things to the Pico board. The board makes things nice for us. It gives us pins that are easy to connect to and pin
names that are easy to remember and well organized. Looking at the pinout diagram for the DS3231 Real Time Clock (RTC) module, I can see that it has four pins that we need to be concerned with. Power and ground of course, then `SCL` and `SDA` as this is an I2C *peripheral*. We want to connect these pins to the `SCL` and `SDA` pins on the Pico. Looking at the [Pico pinout diagram](https://www.raspberrypi.com/documentation/microcontrollers/raspberry-pi-pico.html), we see that `GP1` can be used as `SCL` and `GP0` can be used as `SDA`.

**Note:** Pay attention to pin names when using other microcontroller boards. There are different ways to name pins. However, `framework` can handle those too.

## Staying organized

Use your favorite text editor to organize your notes. Make sure that you have the *spaces* and *colons* in the right places. It will be important later:

    rtc:
      peripheral_type: DS3231RTC
      pins:
        SCL: GP1
        SDA: GP0

On the first line, we have the *alias* that we want to use for the Real Time Clock peripheral. `rtc` seems like a pretty good alias. On the next line, 
we write what type of peripheral it is as there are *lots* of different clocks out there and we need to be specific about which one we're using. Finally, we write what pin connections we made.

Continuing with the rest of our peripherals...

The MAX7219 LED Matrix:

    led_matrix:
      peripheral_type: MAX7219Matrix
      width: 32
      height: 8
      pins:
        SCK: GP2
        MOSI: GP3
        CS: GP4

**Note:** We added some extra information about our led matrix here. We know how many LEDs tall (`height`) it is and how many LEDs wide (`width`) it is, so we write that down too.

A pushbutton:

    button0:
      peripheral_type: DigitalIn
      pin:
        red_button: GP13

And a potentiometer:

    brightness:
      peripheral_type: AnalogIn
      pin:
        pot1: GP27

Finally, our notes all together should look something like this:

    rtc:
      peripheral_type: DS3231RTC
      pins:
        SCL: GP1
        SDA: GP0

    led_matrix:
      peripheral_type: MAX7219Matrix
      width: 32
      height: 8
      pins:
        SCK: GP2
        MOSI: GP3
        CS: GP4

    button0:
      peripheral_type: DigitalIn
      pin:
        red_button: GP13

    brightness:
      peripheral_type: AnalogIn
      pin:
        pot1: GP27

The astute reader will notice that this is [YAML](https://yaml.org/), a way to organize and store information for computers in a human readable way. Unfortunately, `framework` doesn't support reading YAML yet so we have to do one more thing for this to be usable for our purposes. But, this is a great first step.

Now, we need to convert our YAML notes to a format called [JSON](https://www.json.org/json-en.html). `framework` can read and understand JSON and JSON is just a slightly different way of organizing and storing our notes. Copy and paste our peripheral notes into a YAML to JSON converter such as [this one](https://www.convertjson.com/yaml-to-json.htm). Then we can copy and paste the JSON results into a file:

    {
       "rtc": {
          "peripheral_type": "DS3231RTC",
          "pins": {
             "SCL": "GP1",
             "SDA": "GP0"
          }
       },
       "led_matrix": {
          "peripheral_type": "MAX7219Matrix",
          "width": 32,
          "height": 8,
          "pins": {
             "SCK": "GP2",
             "MOSI": "GP3",
             "CS": "GP4"
          }
       },
       "button0": {
          "peripheral_type": "DigitalIn",
          "pin": {
             "red_button": "GP13"
          }
       },
       "brightness": {
          "peripheral_type": "AnalogIn",
          "pin": {
             "pot1": "GP27"
          }
       }
    }

If you look closely, you can see that all of our notes are in the JSON output, they're just formatted a little differently. This is a format that `framework` can read and use. This is our project *configurations* file.

Congratulations! You've completed the first part of your `framework` project! In addition to having a file that `framework` can use, you also have some documentation about how to connect your Pico that others can easily follow to recreate your project.

## Writing our app

One of the nice things about `framework` is that it gives us access to our peripherals by the alias we used in our project configurations file. All we need to do is *import* them into our app. Look back at the YAML and JSON files we made in the previous section. We'll use the aliases we gave to our peripherals in those files:

    from framework import rtc, led_matrix, button0, brightness

This lets us use the features of our peripherals in our app. For example, we want to be able to read the time from `rtc` and write the time to `led_matrix`. We want to be able to act when `button0` is pressed and adjust how bright the display is based on what we know about `brightness`.

On the next line, we want to bring in some tools that make writing an app easier:

    from framework import Application

The last of our imports is a special *library* called `asyncio`. This library is a collection of code that lets our peripherals and app work together:

    import asyncio

Our imports all together. This is the first three lines of our app:

    from framework import rtc, led_matrix, button0, brightness
    from framework import Application
    import asyncio

Now we're ready to write our `App` *class*. A class is a place we can put different pieces of information and functions that work together. Our `App` class will have a way to *run* our app and will *inherit* information and functions from `Application` that we imported from `frk_appbase`:

    class App(Application):
        async def run(self):

The next part is where our code goes. Remember that we want to make a digital clock. When we push the button, we will see the day. When we adjust the potentiometer, the display will get brighter or dimmer:

    class App(Application):
        async def run(self):
            while True:
                led_matrix.text = {(1, 1): f'{rtc.hour:02d}:{rtc.minute:02d}'}
                led_matrix.brightness = 16 * brightness.value // 2**16
                if button0.value:
                    led_matrix.text = {(7, 1): f'{rtc.abr}'}
                await asyncio.sleep(self.sleep)

This looks like a lot to unpack, but we can take it line by line.

    while True:

This lets our app run *forever*. Not really forever, but we want our clock to run all the time. `while` makes a loop in our code that while `True`, will run until we turn off the power.

    led_matrix.text = {(1, 1): f'{rtc.hour:02d}:{rtc.minute:02d}'}

Our `led_matrix` peripheral has a *parameter* called `text`. We can read and write information to parameters. If we save a message to the `text` parameter in the right way, our `led_matrix` will display it.

`rtc` has two parameters that we want to use in this line: `hour` and `minute`. These parameters let us get information from `rtc`. So, first we get the hour from `rtc.hour` and *format* it in the right way. Then we do the same for `rtc.minute`. 

The `(1, 1)` that you see gives `led_matrix` information about the x and y position that the time should be displayed. Finally, the outer `{}` symbols package all the information as a *dictionary* which `led_matrix.text` can use.

    led_matrix.brightness = 16 * brightness.value // 2**16

Next we get the information from our potentiometer with `brightness.value`. This value can be an *integer* value from 0 to 65536, but `led_matrix.brightness` only has 16 settings. So, `16 * brightness.value // 2**16` changes the possibly very large value to a smaller one.

    if button0.value:
        led_matrix.text = {(7, 1): f'{rtc.abr}'}

`button0.value` can only be True if the button is pressed or False if the button is not pressed. So only when `button0` is pressed will `led_matrix.text = {(7, 1): f'{rtc.abr}'}` become active. Similar to the previous time we wrote information to `led_matrix`, we have to package it in the right way. We get the day from `rtc.abr`, format it in the right way, and give the position `(7, 1)`.

    await asyncio.sleep(self.sleep)

The final line gives control to our peripherals so they have time to work. Our app goes to sleep for a very short amount of time then wakes up and continues the `while True` loop.

Our completed app:

    from framework import rtc, led_matrix, button0, brightness
    from framework import Application
    import asyncio

    class App(Application):
        async def run(self):
            while True:
                led_matrix.text = {(1, 1): f'{rtc.hour:02d}:{rtc.minute:02d}'}
                led_matrix.brightness = 16 * brightness.value // 2**16
                if button0.value:
                    led_matrix.text = {(7, 1): f'{rtc.abr}'}
                await asyncio.sleep(self.sleep)

## Preparing CIRCUITPY

`CIRCUITPY` is a little USB drive that CircuitPython uses to store code and other files. It acts exactly like a normal USB drive, though. You can copy and paste files to it or save files from a text editor or Integrated Development Environment (IDE) of your choice... Again, anything you can do with a normal USB drive, `CIRCUITPY` does it too.

When you open your `CIRCUITPY` drive, you might see some files and folders. Usually, there is a `lib` folder. If there isn't a `lib` folder, make one now. This will hold all the files we need to make our peripherals work.

Next, we need to make our project folder. Before we can do that, though, we need to choose a name for our project. `clock` seems like a reasonable name. Make a folder and name it `clock`. This is where we will keep our project files. Save your JSON configurations file from the previous section here. Name it `clock.json`. Save your app file here too. Name it `clock.py`.

Your `CIRCUITPY` drive should now look similar to this. It's no problem if there are other files and folders on your drive. These are the files and folders we're interested in:

```
| CIRCUITPY
|-- clock
|---- clock.json
|---- clock.py
|-- lib
```

Now we need to add some files to our `CIRCUITPY` drive. These are the tools that our app and peripherals will need to run. They are called *dependencies* or *requirements* If you followed the [CircuitPython welcome guide](https://learn.adafruit.com/welcome-to-circuitpython) you might have downloaded an [Adafruit library zip file](https://circuitpython.org/libraries). If you haven't already downloaded the library zip file, download it now and unzip the file in a convenient place on your computer.

Open the `lib` folder from the library zip file and copy the following files or folders to your `CIRCUITPY` drive `lib` folder:

* asyncio
* adafruit_max7219
* adafruit_ds3231.mpy
* adafruit_ticks.mpy

From the `examples` folder, copy this file to your `CIRCUITPY` drive main folder:

* font5x8.bin

Copy from your `framework` folder to your `CIRCUITPY` drive `lib` folder:

* framework.mpy

And from `framework/peripherals`, copy these files to your `CIRCUITPY` drive `lib` folder:

* frk_analogin.mpy
* frk_digitalin.mpy
* frk_max7219matrix.mpy
* frk_ds3231rtc.mpy

Your `CIRCUITPY` drive should now look like this:

```
| CIRCUITPY
|-- clock
|---- clock.json
|---- clock.py
|-- font5x8.bin
|-- lib
|---- asyncio
|---- adafruit_max7219
|---- adafruit_ds3231.mpy
|---- adafruit_ticks.mpy
|---- framework.mpy
|---- frk_analogin.mpy
|---- frk_digitalin.mpy
|---- frk_max7219matrix.mpy
|---- frk_ds3231rtc.mpy
```

## Running your app

The last thing we need to do is make a `code.py` file (or change an existing one) on your `CIRCUITPY` drive:

    from framework import AppLoader
    
    AppLoader(app='clock')
    
Save this file to your `CIRCUITPY` drive and the app should run. You'll see the time (maybe not the correct time...) Push the button and you'll see the day (maybe not the correct day...) Turn the potentiometer and the display will get brighter and dimmer.

Check to see that `code.py` is in the right place:

```
| CIRCUITPY
|-- clock
|---- clock.json
|---- clock.py
|-- code.py
|-- font5x8.bin
|-- lib
|---- asyncio
|---- adafruit_max7219
|---- adafruit_ds3231.mpy
|---- adafruit_ticks.mpy
|---- framework.mpy
|---- frk_analogin.mpy
|---- frk_digitalin.mpy
|---- frk_max7219matrix.mpy
|---- frk_ds3231rtc.mpy
```


**Challenge**: Use the [CircuitPython DS3231 library documentation](https://docs.circuitpython.org/projects/ds3231/en/latest/index.html) to find out how to set the correct date and time.

## Discussion

### Names and Aliases

Names are important in `framework` as they help `framework` find the things it needs to find. The first example is our project name. Our project folder was named `clock`, our configurations file was named `clock.json`, and our app file was named `clock.py`. This is not by coincidence. When `AppLoader` is given an app name it looks for a folder with that same name and expects to find two files in that folder also with the app name but one with `.py` (or `.mpy`) and the other with `.json` extensions.

We should refer to board pins as having names. In our project we used a Raspberry Pi Pico board with pin names like GP0 and GP1. There are other boards that run CircuitPython that use a different naming convention, for example: A0, D1, SCL, SDA, etc.

    rtc:
      peripheral_type: DS3231RTC
      pins:
        SCL: GP1
        SDA: GP0

In order to avoid confusion, we should consider that we alias pin names in our configurations file. This would mean that in this example, the pin names are GP1 and GP0 but the pin aliases are SCL and SDA. So, GP1 is aliased with SCL and GP0 is aliased with SDA. 

We should also say that `rtc` is a peripheral alias in this example, while `DS3231RTC` is a peripheral type and 'frk_ds3231rtc.mpy' is a peripheral name. If you noticed, though, peripheral names and peripheral types are related. We can easily find the peripheral name from the peripheral type and can generally guess the peripheral type from the peripheral name. To try to best follow [Python naming conventions](https://namingconvention.org/python/) while reducing filename confusion, `framework` peripherals names work like this:

    PeripheralType -> frk_peripheraltype
    
Where the peripheral name is prefixed with 'frk_' to show that it is a `framework` peripheral, then the lowercase of the peripheral type. Peripheral names may have either `.py` (for normal Python files) or `.mpy` (for [precompiled Python files](https://docs.micropython.org/en/latest/reference/mpyfiles.html)) extensions.

Aliases should be unique in the space that they have influence. What this means is that peripherals must not have the same alias as any other peripheral and pins of a peripheral must not have the same alias as any other pin in that peripheral. However, there can be different peripherals that have the same pin aliases as one peripheral's pin aliases do not have influence over another peripheral's pin aliases.

We copied a file to the CIRCUITPY `lib` folder in our project with the name 'frk_ds3231rtc.mpy'. That file has a class inside that needs to be used by `framework`. The name of that class *is* the peripheral type. To easily find the name of that class, we can use the [REPL](https://learn.adafruit.com/welcome-to-circuitpython/the-repl). First, start the REPL and import the module using the peripheral name (but not the extension) like so:

    >>> import frk_ds3231rtc

Then we can use the `dir` function to show the class name:

    >>> print(dir(frk_ds3231rtc))

You should see a list of things, but in that list will be our peripheral type `DS3231RTC`. You will see some other things there too, but we can ignore those. Try using this method again for some other modules.

### 'pin' and 'pins'

In the configurations file, `framework` does not differentiate between 'pin' and 'pins'. There is no singular or plural in the configurations file. *Internally*, `framework` counts the pins before using them and assigns 'pin' for one pin and 'pins' for multiple pins (This won't be relevant until we start developing our own peripherals, so don't worry about it too much right now).

There are a lot of peripherals that only need one pin. There are a lot peripherals that have multiple pins. However, there are other peripherals that have specialized pins. We saw some examples of this in our `clock` project. Our pushbutton only needed one generic pin (`GP13`) and a generic pin alias (`red_button`) but `rtc` needed specialized pins and specialized pin aliases (`SDA` and `SCL`). Very often (as is the case for most peripherals without specialized pins) `framework` will ignore the pin alias. However, don't leave the pin alias blank or none. It is still important for other purposes.

In cases with multiple non-specialized pins (eg. keypads) the pin *names* will be reordered in alpha-numeric order (eg. `D1`, `D3`, `A5` -> `A5`, `D1`, `D3`) before being used and the pin aliases will be ignored. But again, don't leave the pin aliases blank in any case.

For specialized pins, the pin alias is very important and attempts to follow standard conventions. For example, the DS3231 is an I2C device and the naming convention for those pins is `SCL` and `SDA`. The pin aliases here specify which pin is `SCL` and which is `SDA` and will be used as such.

#### Contact

wdyn1@mail.com
