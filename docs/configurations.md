# Configurations

A configurations file is a JSON (.json) or messagepack (.mpk) file that is found in the project folder, has same name as the project folder, and contains information about all peripherals needed for a project (ie. a peripheral configuration). There may be zero or more peripheral configurations in a configurations file. At the most base level, a configurations file is a set of nested dictionaries.

Each peripheral configuration has two *requirements*:

* A unique *alias* that identifies the individual peripheral

* A `peripheral_type` that identifies the functionality of the peripheral (does not need to be unique)

*Optionally*, a peripheral configuration may contain:

* Default settings and values

* Initialization information

* `pin` or `pins` aliases and names

For example:

    {
        "board_led": {
            "peripheral_type": "DigitalOut",
	        "enable": true,
	        "drive_mode": "PUSH_PULL",
	        "invert": false,
            "pin": {
                "board_led": "LED"
            }
        },
        "clk": {
            "peripheral_type": "FreeRun",
            "initial_value": false,
            "t_on": 1.0,
            "t_off": 0.5
        }
    }

## "pin" and "pins"

In the configurations file, framework does not differentiate between "pin" and "pins". There is no singular or plural pin in the configurations file. You should expect a different level of importance placed on pin aliases and names depending on the peripheral. In general, peripherals that use single pins will ignore the alias. Also in general, peripherals with more than one pin will place different levels of priority on the aliases and names.

In cases with multiple non-specialized pins (eg. keypads) the pin names will be reordered in alpha-numeric order (eg. D1, D3, A5 -> A5, D1, D3) before being used and the pin aliases and names will be made available by the peripheral.

For specialized pins, the pin alias is very important and attempts to follow standard conventions. For example, for I2C devices the naming convention for those pins is SCL and SDA. The pin aliases here specify which pin is SCL and which is SDA and will be used as such.

However, if used, each pin must have an alias that is unique in its scope and a name that is unique to the configurations file and compatible with the board being used. For example, the [Raspberry Pi Pico](https://www.raspberrypi.com/documentation/microcontrollers/raspberry-pi-pico.html) uses a "GP*" (eg. GP0, GP1, ...) naming convention while other boards like the [Adafruit Grand Central M4](https://learn.adafruit.com/adafruit-grand-central) uses an "A*"/"D*" (eg. A0, A1, D0, D1, ...) naming convention. The naming convention used in the configurations file must match the pin naming convention used on the board. If there is any question about the naming convention used on the board, enter the REPL, `import board`, and `print(dir(board))`. This will show the pin names available on the board.
