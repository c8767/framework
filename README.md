# What is `framework` for CircuitPython?

`framework` for [CircuitPython](https://circuitpython.org/) is an app framework for [microcontrollers running CircuitPython](https://circuitpython.org/downloads) using [cooperative multitasking](https://learn.adafruit.com/cooperative-multitasking-in-circuitpython-with-asyncio). It helps keep projects organized. It loads apps.
It makes peripherals easy and reusable.

`framework` is:

* Small

* Easy to use

* Flexible

Because `framework` runs on top of [CircuitPython](https://circuitpython.org/) >= 7.1.0, make sure [you are familiar with and set up for CircuitPython](https://learn.adafruit.com/welcome-to-circuitpython) before starting. `framework` has been developed for and tested on RP2040, SAMD51, and SAMD21 based microcontroller boards. It has not been tested on Linux devices.

# Installation

This guide assumes you have followed the excellent [Welcome to CircuitPython](https://learn.adafruit.com/welcome-to-circuitpython) tutorial and have a CIRCUITPY drive accessible on your computer.

Clone this repository or download this repository as a zip file and unzip it in a convenient location. Copy the 'framework.mpy' file from the 'framework' folder of this repo to the 'lib' folder on your CIRCUITPY drive. You will also need to copy the 'asyncio' library and 'adafruit_ticks.mpy' module from the [Adafruit libraries bundle](https://circuitpython.org/libraries) to the 'lib' folder on your CIRCUITPY drive.

Check to see that your CIRCUITPY drive looks like the following:

```
| CIRCUITPY
|-- lib
|---- asyncio
|---- adafruit_ticks.mpy
|---- framework.mpy
```

**Note:** There may be other files on your CIRCUITPY drive.

# Usage

There are four steps to making a `framework` project:

1. [Organize your project](https://gitlab.com/c8767/framework/-/blob/main/docs/project_organization.md)

2. [Make a configurations file](https://gitlab.com/c8767/framework/-/blob/main/docs/configurations.md) and connect the peripherals to your board

3. [Write an app](https://gitlab.com/c8767/framework/-/blob/main/docs/application.md)

4. [Load and launch your app](https://gitlab.com/c8767/framework/-/blob/main/docs/running_app.md)

# Hello World (Blink!)

## Getting Organized

The first step to starting a `framework` project is to organize your CIRCUITPY drive. Make sure that you have the 'asyncio' library, the 'adafruit_ticks' module, and the 'framework' module in your 'lib' folder as described in the Installation section of this document. Next make a folder with your project name. For this example, let's use 'blink' as our project name.

We will also be using 'frk_freerun.mpy' and 'frk_digitalout.mpy' for this project, so copy those from the 'framework/peripherals' folder to your 'CIRCUITPY/lib' folder as well.

```
| CIRCUITPY
|-- blink
|-- lib
|---- asyncio
|---- adafruit_ticks.mpy
|---- framework.mpy
|---- frk_digitalout.mpy
|---- frk_freerun.mpy
```

## Configure

The next thing we need to do is make a configurations file. This contains information about our peripherals including the peripheral alias and peripheral type. Using your favorite JSON (or plain text) editor, copy and paste the following configurations and save the file as 'CIRCUITPY/blink/blink.json':

    {
        "board_led": {
            "peripheral_type": "DigitalOut",
            "pin": {
                "my_led": "LED"
            }
        },
        "clk": {
            "peripheral_type": "FreeRun",
            "t_on": 1.0,
            "t_off": 0.5
        }
    }

**Note:** In this example, there isn't any external hardware to connect if you have an onboard LED. However, if your board does not have an onboard LED, connect an LED to a digital pin on your board and change `"LED"` to the name of the pin you have connected to (eg. "GP0", "D1", ...)

## App

The magic of `framework` is that it lets us import our peripherals into our app in a Pythonic way. It uses the peripheral aliases we used in our configurations file. `framework` will also handle the details of making our app run "forever". Using your favorite Python (or plain text) editor, copy and paste the following code and save the file as 'CIRCUITPY/blink/blink.py':

    from framework import board_led, clk

    def loop():
        board_led.value = clk.value

## Load and Launch

Another magic thing that `framework` does is load our app and peripherals in a very simple way. Given our app name, it looks for our project folder, finds our configurations file and loads the peripherals, loads our app, and runs the peripherals and app. This is accomplished using `AppLoader`. Using your favorite Python (or plain text) editor, copy and paste the following code and save the file as 'CIRCUITPY/code.py':

    from framework import AppLoader

    AppLoader(app='blink')

**Note:** If you are using the default CircuitPython settings that resets when there is a change on your CIRCUITPY drive, you should see a blinking LED on your board. If not, you may need to manually reset your board following the instructions specific to your board to do so.

# Next Steps

* Learn how to [set up your own peripherals](https://gitlab.com/c8767/framework/-/blob/main/docs/peripherals.md)

* Use the [debugger](https://gitlab.com/c8767/framework/-/blob/main/docs/debugger.md)

* Check the [execution time of your code](https://gitlab.com/c8767/framework/-/blob/main/docs/analyzer.md)
